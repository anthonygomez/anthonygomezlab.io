// Data format : ['ID span selector', older date, 'prefix?', 'suffix?']

var id_date_data = [
    ['age', new Date(1996, 9, 29), false,true],
    ['beginXP', new Date(2012, 9, 1), false,false],
    ['sti2d-1', new Date(2012, 9, 1), true,true],
    ['sti2d-t', new Date(2013, 9, 1), true,true],
    ['bts-sio-1-1', new Date(2014, 9, 1), true,true],
    ['bts-sio-1-2', new Date(2015, 9, 1), true,true],
    ['bts-sio-2', new Date(2016, 9, 1), true,true],
    ['l3-miage', new Date(2017, 9, 1), true,true],
    ['m1-miage', new Date(2018, 9, 1), true,true],
    ['m2-miage', new Date(2019, 9, 1), true,true]
];

function runTimeDiffLoader() {

    id_date_data.forEach(id_date_data => {
        elements = document.getElementsByClassName(id_date_data[0]);

        for (let index = 0; index < elements.length; index++) {

            if (typeof (document.getElementsByClassName(id_date_data[0])[index]) != 'undefined' && document.getElementsByClassName(id_date_data[0])[index] != null) {
                var years = Math.abs(new Date(Date.now() - id_date_data[1].getTime()).getUTCFullYear() - 1970);
                
                var prefix = "";
                if (id_date_data[2]) {
                    prefix = "+ ";
                    if (years < 1) {
                        years = 1;
                        prefix = "< "
                    }
                }

                var suffix = "";
                if (id_date_data[3]) {
                    var suffix = " an";
                    if (years > 1) {
                        suffix = suffix + "s";
                    }
                }
                document.getElementsByClassName(id_date_data[0])[index].innerHTML = prefix + years + suffix;
            }
        }
    })
}

// years diff

runTimeDiffLoader();

document.getElementsByClassName('dev-count')[0].innerHTML = document.querySelectorAll('.dev').length;
document.getElementsByClassName('finish-count')[0].innerHTML = document.querySelectorAll('.online, .offline').length;

// ideas projects counter from Trello API

var _0xc39f=['/lists/5eb28483826be0469f61de48/cards?','key=','&token=','e7b1949f1afffead246131527ee6f80f258406a4ce920a71c86d925dd3024956','length','innerHTML','idea-count'];(function(_0x4afa0d,_0xc39f13){var _0x975899=function(_0x5bebb7){while(--_0x5bebb7){_0x4afa0d['push'](_0x4afa0d['shift']());}};_0x975899(++_0xc39f13);}(_0xc39f,0x19f));var _0x9758=function(_0x4afa0d,_0xc39f13){_0x4afa0d=_0x4afa0d-0x0;var _0x975899=_0xc39f[_0x4afa0d];return _0x975899;};var _0x218f67='https://api.trello.com/1';var _0x3319fb=_0x9758('0x5');var _0x45b5ff='b53182106ba7da192a1f376871241303';var _0x5488c5=_0x9758('0x1');var _0x3cbcfa=_0x218f67+_0x3319fb+_0x9758('0x6')+_0x45b5ff+_0x9758('0x0')+_0x5488c5;$['getJSON'](_0x3cbcfa,function(_0x9bc82c){document['getElementsByClassName'](_0x9758('0x4'))[0x0][_0x9758('0x3')]=_0x9bc82c[_0x9758('0x2')];});

// year copyright

document.getElementById('year').innerHTML = new Date().getFullYear();

// Created by Anthony Gomez